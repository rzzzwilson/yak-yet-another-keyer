YAK
===

This is yet another keyer using a small microcontroller.  It's based
on the original Yack keyer at https://sourceforge.net/projects/yack/.
This version is based on the version described at:

http://blog.templaro.com/a-tiny-and-open-source-cw-keyer/
